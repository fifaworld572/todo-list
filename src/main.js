import Vue from 'vue';
import store from './store';
import App from './App.vue';
import componentsUI from '@/components/UI';
import functionGlobal from "./globalFunction/handlingString";

functionGlobal.forEach( functionMe => {
  Vue.prototype['$' + functionMe.name] = functionMe;
})

Vue.config.productionTip = false;

componentsUI.forEach( component => {
  Vue.component(component.name, component);
})


new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
