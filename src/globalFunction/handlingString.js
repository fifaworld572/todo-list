export function getUrl (pet) {
    let images = require.context('../assets/img/iconSite/', false);
    return images('./' + pet);
}

function checkEntity(value,required = false,minLength = 0) {
    if(required && !value) {
        return {
            errorInput:true,
            textError: 'Обязательно поле'
        }
    }

    if(minLength > value.length) {
        return {
            errorInput:true,
            textError: `Мин.количество символов - ${minLength}, сейчас - ${value.length}`
        }
    }

    return {
        errorInput:false
    }
}

export default [getUrl,checkEntity];