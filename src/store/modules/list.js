export default {
    namespaced: true,
    state: () =>({
        showWindows: false
    }),

    mutations: {
        setShowWindow(state, value) {
            state.showWindows = value;
        }
    },
}